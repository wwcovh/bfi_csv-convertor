## Installation

1. Install [Python 3 for Windows](https://www.python.org/downloads/), don't forget to add Python to PATH (the installer prompts for this at the end of installation)
2. Download and unzip [Git for Windows](https://git-scm.com/download/win) to ``C:\Tools`` (so the path to the binary is ``C:\Tools\Git\bin\git.exe``)
3. ``git clone`` the repository into ``C:\Tools``
4. Install dependencies:
    - ``pip install lxml transliterate``