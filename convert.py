#!/usr/bin/env python3

## Input row example
## A ;B ;;D;E            ;F                ;G   ;H                                                              ;I     ;J       ;K                        ;L               ;M     ;N            ;O          ;P    ;;R           ;S           ;;U    ;;;;;;;;;AD                    ;AE    ;AF ;AG   ;AH   ;AI
## RU;CN;;U;UY185142916CZ;TYZPH0007246122YQ;Iris;Apartment, Street, City, District, Country;ZIP;City;Firstname Lastname;Street 123;ZIP;City;TYCZ0003518;2,853;;928-220-1320;928-220-1320;;0,033;;;;;;;;;chunmo(description);HSCode;CurrencyCode;3,011;0,033;CN

## Output rows example
## RB004740765CZ;Lastname Firstname;Street 123;City;ZIP;CountryCode;RB;0,6;53+9;;;;;CurrencyCode;;;;11
## ;;;;;;;;;A;1;0,5;1;;CN;HScode;description;

## Transformation rule
## Row 1 (OLZ):
## E;K;L;N;M;A;"UY";U;"61+9";"";"";"";"";AF;;;;"11"
##
## Row 1 (RR):
## E;K;L;N;M;A;"RB";U;"53+9";"";"";"";"";AF;;;;"11"
##
## Row 2:
## ;;;;;;;;;"A";"1";AH;AG;;AI;AE;AD;
##
## ver 0.1.2

import re
import sys
import transliterate
from resources.us_codes import us_codes

def create_output_line_la(columns):
  '''
  Method for create output line in LA mode
  '''
  # if len(columns) != 40:
  #   sys.exit("[ERROR] The row %i has %i columns, but it should have 40. Exiting program." % (row_number, len(columns)))  

  columns.pop(1)

  line_out = []
  unique_id = columns[5].replace(columns[5][0:2], "LA")
  line_out.append(unique_id)                      # Unique ID

  name = re.sub(r"(\w+)([A-Z]+)", r"\1 \2", columns[10])
  line_out.append(name.strip())                   # Recipient Name

  address = re.sub(r"(\w)([A-Z0-9]+)", r"\1 \2", columns[11])
  line_out.append(address)                        # Street with house number

  zip = columns[12]
  if len(zip) < 5:
    zip = zip.zfill(5)
  line_out.append(zip)        # Recipient Zip Code

  city = re.sub(r"(\w+)([A-Z0-9]+)", r"\1 \2", columns[13])
  line_out.append(city)                           # Recipient City
  
  line_out.append(columns[0])                     # Recipient Country
  line_out.append('"' + columns[18] + '"')        # Recipient Phone Number

  email_regex = '[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}'
  if re.fullmatch(email_regex, columns[17].strip()):
    line_out.append(columns[17].strip())        # Recipient E-mail
  else:
    line_out.append("")

  line_out.append(columns[20].replace(".",","))        # Weight of consignment
  line_out.append("LA")               # Type  of consignment –  LA
  line_out.append("9")                # Additional Services – 9 (9 is for priority)

  f_out.write(";".join(line_out) + "\r\n")
  return 0

mode = sys.argv[1]
if (mode not in ["olz", "rr", "lf", "la"]):
  print('Error: wrong file format specified, please use either "olz", "rr", "lf" or "la"')
  exit(1)

f_czech_post_hs_codes = open("resources/ZV_CEL_OBSAH_NAZVY.csv", "r", encoding="utf-8")
czech_post_hs_codes = set()
for line in f_czech_post_hs_codes:
  czech_post_hs_codes.add(line.split(";")[0])

f_in = open(sys.argv[2], "r", encoding="utf-8")
f_out = open(sys.argv[2][:-4] + "_out.csv", "w", encoding="utf-8", newline='')

row_number = 0

for line_in in f_in:

  if row_number < 0:
    break
  else:
    row_number += 1

  # hebrew_conv = romanize3.__dict__['heb']
  # if hebrew_conv.filter(line_in).strip() != "":
    # print(hebrew_conv.filter(line_in).strip())
    # print(line_in)
    # pass

  columns = transliterate.translit(transliterate.translit(line_in.strip(), 'ru', reversed=True), 'mk', reversed=True).split(";")
  # for column in columns:
    # if hebrew_conv.filter(column).strip() != "":
      # print(column)
      # print(hebrew_conv.convert(column))
      # print()

  if mode == "la":
    line_out = create_output_line_la(columns)
    continue

  no_of_columns = len(columns)
  if no_of_columns != 36:
    sys.exit("[ERROR] The row %i has %i columns, but it should have 36. Exiting program." % (row_number, no_of_columns)) 

  print("[Row number: " + str(row_number) + "] \r\nInput:\r\n" + str(columns) + "\r\n")
  # print("[Row number: " + str(row_number) + "]")

  line1_out = []
  line1_out.append(columns[5])                                # Column E: Barcode
  line1_out.append(columns[11])                               # Column K: Recipient Name
  line1_out.append(columns[12])                               # Column L: Recipient Address Line
  line1_out.append(columns[14])                               # Column N: Recipient City
  line1_out.append("'" + columns[13])                         # Column M: Recipient ZIP

  line1_out.append(columns[0])                                # Column A: DestPostOrg Code

  province = columns[1].strip().lower()                       # Column B: Province (US state)
  if province == "":
      line1_out.append("")
  elif province in us_codes.keys():
    line1_out.append(us_codes[province])
  else:
    line1_out.append(columns[1].strip())

  gross_weight = float(columns[21].replace(",", "."))           # Column U: Gross Weight
  gross_weight += 0.001                                         # Adjusted by +0.001 (one gram)
  line1_out.append(str("%.3f" % gross_weight).replace(".",","))

  if (mode == "olz"):
    line1_out.append("61+9")
  elif (mode == "rr"):
    line1_out.append("53+9")
  elif (mode == "lf"):
    line1_out.append("2F")
  
  if (mode == "lf"):
    line1_out.append("LF")
  else: 
    line1_out.append("")
  line1_out.append("")
  line1_out.append("")
  line1_out.append("")
  line1_out.append(columns[32])                               # Column AF: Currency Code
  line1_out.append("")
  line1_out.append("")
  line1_out.append("")
  line1_out.append("11")

  line2_out = []
  for i in range(0,9):
    line2_out.append("")

  line2_out.append("A")                                       # Column A: DestPostOrg Code
  line2_out.append("1")
  line2_out.append(columns[34].replace(".",","))              # Column AH: Net Weight
  line2_out.append(columns[33].replace(".",","))              # Column AG: Shipment value
  line2_out.append("")
  line2_out.append(columns[35])                               # Column AI: Origin Country Code

  source_hs_code = columns[31][:6]
  # print(source_hs_code)
  first_four_digits = source_hs_code[:4]
  codes_to_replace = {
    "3304": "99",
    "3305": "90",
    "8471": "70",
  }
  if (first_four_digits in list(codes_to_replace.keys())):
    output_hs_code = first_four_digits + codes_to_replace.get(first_four_digits, "Invalid HS Code")
  elif (source_hs_code in czech_post_hs_codes):
    output_hs_code = source_hs_code
  else:
    # print(str(first_four_digits))
    # print(source_hs_code)
    output_hs_code = "330499"

  line2_out.append(output_hs_code)                            # Column AE: HS Code

  line2_out.append(columns[30])                               # Column AD: Product description
  line2_out.append("")

  print("Output:")
  print(";".join(line1_out))
  print(";".join(line2_out) + "\r\n")
  print("=============================================================================")

  f_out.write(";".join(line1_out) + "\r\n")
  if (mode != "lf"):
    f_out.write(";".join(line2_out) + "\r\n")

print("\r\nAll CSVs processed without an error.")

f_in.close()
f_out.close()
